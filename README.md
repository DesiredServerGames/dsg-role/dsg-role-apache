# Ansible Role: Apache 2.x

An Ansible Role that installs Apache 2.x on RHEL/Fedora, Debian/Ubuntu

## Requirements

none

## Role Variables

    apache_listen_ip: "*"
    apache_listen_port: 80
    apache_listen_port_ssl: 443

## Dependencies

None.

## Example Playbook

    - hosts: webservers
      vars_files:
        - vars/main.yml
      roles:
        - { role: dsg-role-apache }

*Inside `vars/main.yml`*:

    apache_listen_port: 8080

## License

MIT 

## Author Information

This role was created in 2014 by [Jeff Geerling](https://www.jeffgeerling.com/), author of [Ansible for DevOps](https://www.ansiblefordevops.com/).
Modified by Didier MINOTTE